---
title: ODOT popsim example
author: joe.broach@oregonmetro.gov
date: 2019-10-14
---

# Overview

PopulationSim (hereafter PopSim) is a population synthesizer commissioned by
ODOT for use in modeling applications. A synthetic population is a complete
collection of fake households and persons residing in real geographies. They
are created to maximize in some way various control totals. PopSim is notable
for its ability to fit control totals at varying nested geography levels.

This project creates an illustrative synthetic (fake) population from a
made up set of inputs--twice the fakeness! The goals are:

    1) Document the mechanics of installing and running PopSim
    2) Demonstrate the relationships among *control totals*,
    *seed populations*, and *synthetic populations*.
    3) Point out some things to watch for in the inputs and outputs, and
    some surprises suggesting further investigation, or at least an
    appropriate wariness on the part of users.

Obtaining or creating appropriate inputs is beyond the scope of this
exploration (for now). I suggest contacting the maintainers of PopSim (or
if you're lucky enough to be at Portland Metro, ask Maribeth).

# Getting PopSim Up and Running
PopSim needs to run in a specific Python environment. While there are sneaky
ways around it, I recommend installing PopSim in its own little environment
to run. An easy way to do this is by installing some flavor of the Python
environment manager Anaconda (get it, Python...Anaconda?). Anaconda lets you
set up any number of Python environments without interfering with any Pythons
already lurking on your computer (like the one ArcGIS stalls, for instance),
and without interfering with one another. Miniconda is nice and lightweight.
Get it at [https://docs.conda.io/en/latest/miniconda.html](https://docs.conda.io/en/latest/miniconda.html). You'll want version 3.x. Other than that, you can
follow the nice instructions at [https://rsginc.github.io/populationsim/getting_started.html#installation](https://rsginc.github.io/populationsim/getting_started.html#installation).

# Setting Up a Test Run

## Choose Geography Levels
PopSim uses the concept of nested hierarchical geographies. At the top level
is the *Meta* geography, and like champions there can be only one. This might
be a region. Next is the *Seed* level at which the starting sample population
is defined (often will be a Census Public Use Microdata Area, or PUMA). Below
that can be any number of *Sub-seed* geographies such as Census Tracts or
Traffic Analysis Zones (TAZs).

For this example, we'll use 4 levels:

    * Meta: REGION
    * Seed: PUMA (in this case, only one Seed level is specified)
    * Sub-seed #1: TRACT
    * Sub-seed #2: TAZ

## Provide a Geography Crosswalk Table
Notice how the levels nest within the hierarchy:

TAZ	| TRACT	| PUMA | REGION
--- | ----- | ---- | ------
1	  | 1	    | 1    | 1   
2   | 1     | 1    | 1   
3   | 2     | 1    | 1
4   | 2     | 1    | 1
... | ...   | ...  | ...

The full crosswalk table with 10 TAZs, 5 tracts, 1 PUMA and 1 Region is
provided in **geo_crosswalk.csv**.

## Provide Controls
Control totals at various geographies set the goals for the synthesizer. The
only "mandatory" match is the number of households at the lowest (smallest)
geography. All other controls are treated as error minimization targets.

In our example, we provide the following controls at various levels:

  * Region: total transit pass holders
  * Tract: # college students, a "RARE" attribute, present in just 10
    cases in a population of 50,000 (0.02%)
  * TAZ: # female, HHBASE # households (must be specified at lowest level)

See example tables in **control_totals_meta.csv**,
**control_totals_tract.csv**, and **control_totals_taz.csv**.

## Provide Seed Households and Persons
Tables of seed households and persons to serve as initial information for the
synthesizer. Note that there can be attributes that are "uncontrolled," such
as car ownership (CAROWN) here, present in 30% of seed HHs. The synthesizer
will still try to "fit" these attribute distributions from the seed tables (if
they're specified in the run configuration), but it is not constrained to do
so.

See the **seed_households.csv** and **seed_persons.csv** files.

## Edit the Config Files
There are 3 configuration files that PopSim looks for in the **config**
folder before a run:

  * **settings.yaml**
  * **controls.csv**
  * **logging.yaml**

Note: yaml stands for Yet Another Markup Language and is just a text file of
settings in a certain format. Your best bet is to copy the format of an
existing file. Here's an example section of the **settings.yaml** file used
here, telling PopSim which attributes to synthesize for HHs and persons:

```yaml
# Synthetic Population Output Specification
# ------------------------------------------------------------------
#

output_synthetic_population:
  household_id: household_id
  households:
    filename: synthetic_households.csv
    columns:
      - CAROWN
  persons:
    filename: synthetic_persons.csv
    columns:
      - per_num
      - FEMALE
      - TRANPASS
      - COLLEGE
      - LICENSE
      - RARE
```

The **controls.csv** is possibly the most complex to get right. Below are the
settings for this example:

```csv
target,geography,seed_table,importance,control_field,expression
num_hh,TAZ,households,1000000000,HHBASE,(households.WGTP > 0) & (households.WGTP < np.inf)
FEMALE,TAZ,persons,500,FEMALE,persons.FEMALE == 1
COLLEGE,TRACT,persons,500,COLLEGE,persons.COLLEGE == 1
TRANPASS,REGION,persons,500,TRANPASS,persons.TRANPASS == 1
RARE,TRACT,persons,500,RARE,persons.RARE == 1
```

The initial fields are self-explanatory (hopefully). The *importance* field
specifies a relative importance for matching a particular control total.
There isn't much guidance on setting these values. The *expression* is
to be provided in Python syntax to define the constraint. In this case, the
control variables are binary, so the value in the expression doesn't much
matter. Note the household weight constraint in the *num_hh* field. This
was copied from the example file and other ranges (or omitting) haven't been
tested.

**logging.yaml** is straightforward.

## Running PopSim
Note: a new run of PopSim will overwrite all outputs, so be sure that's what
you want, or else copy to another directory first!

To run a configured PopSim design in a conda environment, open an Anaconda
terminal prompt (e.g. Windows -> Anaconda Prompt):

```
activate popsim
```
where popsim is the name of the popsim conda environment you created during
the install. Use cd commands to navigate to your popsim run directory, and
then simply enter:

```
python run_populationsim.py
```
This will run the instance in the folder you navigated to, providing status
updates along the way.

## Outputs
Outputs will be written to the outputs/ folder. Results appear to be
deterministic for given input/setting combinations, but this has not been
confirmed.

Let's start with the summary files:

**summary_TAZ_PUMA.csv**

geography | id | num_hh_control | FEMALE_control | COLLEGE_control | TRANPASS_control | RARE_control | num_hh_result | FEMALE_result | COLLEGE_result | TRANPASS_result | RARE_result | num_hh_diff | FEMALE_diff | COLLEGE_diff | TRANPASS_diff | RARE_diff
--- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | --- | ---
PUMA | 1 | 40000 | 51000 | 10000 | 5000 | 10 | 40000 | 51086 | 9997 | 4997 | 10 | 0 | 86 | -3 | -3 | 0

This file shows the control total, the "result" or synthetic total, and the
difference between them over the entire meta area, in this case the region.
Note how only *num_hh* is binding. The other totals are extremely close, with
the FEMALE total showing the greatest difference (86/51000, or 0.2%).

**summary_TRACT.csv** and **summary_TAZ.csv** follow suit.

**synthetic_households.csv** and **synthetic_persons.csv** contain the
generated populations.

It's worth noting how rare conditions can impact the results. In our example,
we specified the attribute RARE occurring in such a small number of people
that it occurred only once in our seed sample:

SERIALNO | SPORDER | PUMA | wgtp | FEMALE | TRANPASS | COLLEGE | LICENSE | hh_id | RARE
--- | --- | --- | --- | --- | --- | --- | --- | --- | ---
1 | 1 | 1 | 10 | 1 | 1 | 1 | 1 | 1 | 1

Note how the synthesizer simply expands the same seed person 10x to meet the
RARE control total; thus, all with the RARE attribute will be female, college
student, etc:

PUMA | TRACT | TAZ	| household_id |	per_num	| FEMALE |	TRANPASS | COLLEGE | LICENSE |	RARE
--- | --- | --- | --- | --- | --- | --- | --- | --- | ---
1	| 1 | 1 |	2481 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 1 |	2482 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 1 |	2483 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 1 |	2484 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 1 |	2485 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 2 |	6481 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 2 |	6482 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 2 |	6483 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 2 |	6484 | 1 | 1 | 1 | 1 | 1 | 1
1	| 1 | 2 |	6485 | 1 | 1 | 1 | 1 | 1 | 1
