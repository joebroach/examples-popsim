In this run, a RARE condition is specified as occurring just once in the seed sample
with a control total of 10 confined to a single census tract.

The synthetic person sample over-represents females by a small amount (<1%).